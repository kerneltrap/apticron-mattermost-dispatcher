# Apticron Mattermost Dispatcher

`apticron-mattermost-dispatcher` allows you to receive `apticron` messages in [mattermost](https://mattermost.com/). It's quite simple and supports only-plain text messages.

## Configure

* Use mattermost UI to create a webhook URL that will be used in the next step.
* You must have a configured SMTP server. It's up to you which one you choose. Create an unique alias in `/etc/aliases` and replace `<webhookURL>`.

```
bLvSDN6Nhkev@domain.tld "| /usr/local/bin/apticron-mattermost-dispatcher --webhook-url <webhookURL> --syslog"
```
* Apply the following settings into `/etc/apticron/apticron.conf`

```
EMAIL="bLvSDN6Nhkev@domain.tld"
DIFF_ONLY="0"
LISTCHANGE_PROFILE=""
ALL_FQDNS="0"
SYSTEM=$(/bin/hostname -f)
IPADDRESSNUM="1"
IPADDRESSES=""
NOTIFY_HOLDS="1"
NOTIFY_NEW="1"
NOTIFY_NO_UPDATES="0"
GPG_ENCRYPT="0"
```
