#!/bin/sh

basename=$(basename $(dirname "$(readlink -f $0)"))

docker run --rm -v "$(pwd)":"/usr/src/$basename" -w "/usr/src/$basename" golang \
  go build -v -o $basename-$(uname -s)
