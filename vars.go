package main

import "flag"

var (
	webhookUrl = flag.String("webhook-url", "http://localhost:8080/uri", "URL to send the data to")
	useSyslog  = flag.Bool("syslog", false, "use syslog to log messages")
	verbose    = flag.Bool("verbose", false, "enable verbose mode")
)

func init() {
	flag.Parse()
}
