package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"log/syslog"
	"net/http"
	"net/mail"
	"os"
)

type Attachment struct {
	Fallback string `json:"fallback"`
	Color    string `json:"color"`
	Pretext  string `json:"pretext"`
	Text     string `json:"text"`
}

type Attachments []Attachment

func main() {
	if *useSyslog {
		logwriter, err := syslog.New(syslog.LOG_LOCAL0, "apticron-mattermost-dispatcher")
		if err == nil {
			log.SetOutput(logwriter)
			log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))
		}
	}

	reader := bufio.NewReader(os.Stdin)
	m, err := mail.ReadMessage(reader)
	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(m.Body)
	if err != nil {
		log.Fatal(err)
	}

	var subject string
	if m.Header.Get("Subject") != "" {
		subject = m.Header.Get("Subject")
	} else {
		subject = ""
	}

	object := map[string]Attachments{
		"attachments": Attachments{
			Attachment{
				subject,
				"#FF8000",
				fmt.Sprintf("**%s**", subject),
				string(body),
			},
		},
	}
	payload, _ := json.Marshal(object)

	response, err := http.Post(*webhookUrl, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()
	log.Printf("mattermost response status code: %s", response.Status)
	if *verbose {
		responseBody, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(string(responseBody))
	}
}
